const { startGetAndLockTransaction } = require("../sessions");

// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => async (context) => {
  await startGetAndLockTransaction(context, options.collections);

  return context;
};
