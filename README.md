# @lpgroup/feathers-mongo-hooks

Collection of hooks to use togheter with fethers-mongo

## Install

Installation of the npm

```sh
echo @lpgroup:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
npm install @lpgroup/feathers-mongo-hooks
```

## Development

When developing this NPM your can patch yup to give better error message.

## Example

```javascript
const yup = require("@lpgroup/yup");
const schema = { key: yup.string().required() };
```

## Contribute

See https://gitlab.com/lpgroup/lpgroup/-/blob/master/README.md

## License

MIT, see LICENSE.MD
